﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Question1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SubmitCoordinates(object sender, EventArgs e)
        {
            int x, y;
            x = Convert.ToInt32(txtxvalue.Text);
            //txtxvalue.Text = x.ToString();
            y = Convert.ToInt32(txtyvalue.Text);

            if (x > 0 && y > 0)
            {
                outputData.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 1";
            }
            else if (x < 0 && y > 0)
            {
                outputData.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 2";
            }
            else if (x < 0 && y < 0)
            {
                outputData.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 3";
            }
            else if (x > 0 && y < 0)
            {
                outputData.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 4";
            }
            else if (x == 0 || y == 0)
            {
                outputData.InnerHtml = "Values of x or y should be greater than 0";
            }
            else
            {
                outputData.InnerHtml = "Enter valid input";
            }
        }
        protected void ClearCoordinates(object sender, EventArgs e)
        {
            txtxvalue.Text = "";
            txtyvalue.Text = "";
        }
    }
}