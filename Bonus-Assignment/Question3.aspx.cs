﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Question3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckIfPalidrome(object sender, EventArgs e)
        {
            string palidrome, revs="";
            palidrome = (txtpalindrome.Text).ToLower();
            palidrome = palidrome.Replace(" ","");


            for (int i = palidrome.Length - 1; i >= 0; i--)
            {
                revs += palidrome[i].ToString();
            }
            if (revs == palidrome)
            {
                outputData.InnerHtml = "The String is Palidrome";
            }
            else
            {
                outputData.InnerHtml = "The String is not Palidrome";
            }
        }

        protected void ClearValues(object sender, EventArgs e)
        {
            txtpalindrome.Text = "";
        }
    }
}