﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Question1.aspx.cs" Inherits="Bonus_Assignment.Question1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Question 1</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="lblxvalue" Text="X Value"></asp:Label>
            <asp:TextBox runat="server" ID="txtxvalue"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                ControlToValidate="txtxvalue"
                Display="Static"
                ForeColor="Red"
                ErrorMessage="* Enter value of x"
                runat="server" />
            <asp:RegularExpressionValidator ID="regexxvalue"
                ControlToValidate="txtxvalue" runat="server"
                ErrorMessage="Only Numbers allowed"
                ValidationExpression="^[+-]?([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">
            </asp:RegularExpressionValidator>
        </div>
        <div>
            <asp:Label runat="server" ID="lblyvalue" Text="Y Value"></asp:Label>
            <asp:TextBox runat="server" ID="txtyvalue"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                ControlToValidate="txtxvalue"
                Display="Static"
                ForeColor="Red"
                ErrorMessage="* Enter value of y"
                runat="server" />
            <asp:RegularExpressionValidator ID="regexvvalue"
                ControlToValidate="txtyvalue" runat="server"
                ErrorMessage="Only Numbers allowed"
                ValidationExpression="^[+-]?([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">
            </asp:RegularExpressionValidator>
        </div>
        <asp:Button runat="server" ID="btnsubmit" Text="Submit Coordinates" OnClick="SubmitCoordinates" />
        <asp:Button runat="server" ID="btnclear" Text="Clear Coordinates" OnClick="ClearCoordinates" />
        <div id ="outputData" runat="server">
            
        </div>
    </form>
</body>
</html>
