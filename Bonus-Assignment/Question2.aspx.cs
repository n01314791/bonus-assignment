﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Question2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckIfPrime(object sender, EventArgs e)
        {
            int count = 0;
            int x;
            x = Convert.ToInt32(txtintvalue.Text);
            for (int i = 2; i <= (x / 2); i++)
            {
                if (x % i == 0)
                {
                    count=1;
                    break;
                }
            }
            if (count == 0)
            {
                outputData.InnerHtml = x + " is prime number";
            }
            else
            {
                outputData.InnerHtml = x + " is not prime number";
            }
        }

        protected void ClearValues(object sender, EventArgs e)
        {
            txtintvalue.Text = "";
        }
    }
}