﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Question2.aspx.cs" Inherits="Bonus_Assignment.Question2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Question 2</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="lblintvalue" Text="Enter a integer"></asp:Label>
            <asp:TextBox runat="server" ID="txtintvalue"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                ControlToValidate="txtintvalue"
                Display="Static"
                ForeColor="Red"
                ErrorMessage="* Enter value of x"
                runat="server" />
            <asp:RegularExpressionValidator ID="regexxvalue"
                ControlToValidate="txtintvalue" runat="server"
                ErrorMessage="Only Numbers allowed"
                ValidationExpression="^[+-]?([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">
            </asp:RegularExpressionValidator>
            <div>
                <asp:Button runat="server" ID="btnsubmit" Text="Check If Prime" OnClick="CheckIfPrime" />
                <asp:Button runat="server" ID="btnclear" Text="Clear Values" OnClick="ClearValues" />
            </div>
            <div runat="server" id="outputData">
            </div>
        </div>
    </form>
</body>
</html>
