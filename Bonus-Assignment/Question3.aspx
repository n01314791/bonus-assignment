﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Question3.aspx.cs" Inherits="Bonus_Assignment.Question3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="lblpalindrome" Text="Input a string of character"></asp:Label>
            <asp:TextBox runat="server" ID="txtpalindrome"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                ControlToValidate="txtpalindrome"
                Display="Static"
                ForeColor="Red"
                ErrorMessage="* Enter value"
                runat="server" />
            <%--<asp:RegularExpressionValidator ID="regexxvalue"
                ControlToValidate="txtintvalue" runat="server"
                ErrorMessage="Only Numbers allowed"
                ValidationExpression="^[+-]?([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">
            </asp:RegularExpressionValidator>--%>
            <div>
                <asp:Button runat="server" ID="btnsubmit" Text="Check If Palidrome" OnClick="CheckIfPalidrome" />
                <asp:Button runat="server" ID="btnclear" Text="Clear Values" OnClick="ClearValues" />
            </div>
            <div id="outputData" runat="server">
                <%--<asp:Label runat="server" ID="lbloutput" Text=""></asp:Label>--%>
            </div>
        </div>
    </form>
</body>
</html>
